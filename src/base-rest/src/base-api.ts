import express, { Router, urlencoded, json } from 'express';
import cors from 'cors';
import { BaseAPICntxt, ApiArgs, ResponseMapperCntxt, NtExpress, NtRouter, CorsOptions, BaseObject } from './types';
import Config from './config';
import Logger from './logger';
import StaticController from './static-controller';
import { StdErrors } from './exceptions';

const LOG_SCOPE = 'API.Base';

export default abstract class BaseApi implements BaseAPICntxt {
  readonly app: NtExpress = express();
  readonly responseMapper!: ResponseMapperCntxt;
  appName: string;
  appVersion: string;
  logger!: Logger;

  private readonly _config: Config;
  private corsConfig!: CorsOptions;

  protected constructor(args: ApiArgs) {
    process.env.PWD = process.cwd();

    this.appName = args.meta.appName;
    this.appVersion = args.meta.appVersion;
    this.logger = new Logger(this.appName);

    this._config = new Config(this.logger);

    const config = args.configOpt || {};

    this.config.load(config.validator, config.location);
    this.responseMapper = args.responseMapper;
  }

  protected setCorsOpt(corsOpt: CorsOptions): void {
    this.corsConfig = corsOpt;
  }

  public get config(): Config {
    return this._config;
  }

  setup(): void {
    throw new Error('setup not implemented');
  }

  initComponents(): void {
    throw new Error('initComponents not implemented');
  }
  initDatasource(): void {
    throw new Error('initDatasource not implemented');
  }
  initRepositories(): void {
    throw new Error('initRepositories not implemented');
  }
  initServices(): void {
    throw new Error('initServices not implemented');
  }
  initRouter(): NtRouter | void {
    throw new Error('initRouter not implemented');
  }

  createRouter = (): NtRouter => Router();

  protected boot() {
    this.setup();
    this.initComponents();
    this.initDatasource();
    this.initRepositories();
    this.initServices();
  }

  protected start() {
    this.boot();

    const staticController = new StaticController({
      appName: this.appName,
      appVersion: this.appVersion,
      appContext: this,
      debug: process.env.DEBUG === 'true',
    });

    // Middlewares for handling request
    this.app.use(json());
    this.app.use(urlencoded({ extended: true }));

    // Enable some cors features
    const allowedOrigins = this.corsConfig.origin;
    if (Array.isArray(allowedOrigins)) {
      this.corsConfig.origin = (o: string | undefined, callback: (err: Error | null, origin?: any) => void) => {
        if (!o) {
          return callback(null, o);
        }

        if (allowedOrigins.indexOf(o) !== -1) {
          callback(null, true);
        } else {
          callback(StdErrors.ERROR_BAD_GATEWAY.compose(`Access to fetch at '${o}' has been blocked by CORS`));
        }
      };
    }

    this.app.use(cors(this.corsConfig));

    const serverPath = this.config.get('server.path');
    // Consume routers
    this.app.use(serverPath, this.initRouter() as NtRouter);

    // Default router for handling not found & error
    this.app.get(serverPath, staticController.handleAPIStatus);
    this.app.use(staticController.handleResourceNotFound);
    this.app.use(staticController.handleError);

    const port = this.config.get('server.port');
    this.app.listen(port, () => {
      this.logger.info(`${this.appName} ${this.appVersion} Server has started. Listening on port ${port}`, {
        scope: LOG_SCOPE,
      });
    });
  }
}
