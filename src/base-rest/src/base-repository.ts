import { BaseObject, BaseRepositoryCntxt } from './types';

abstract class BaseRepository<D> implements BaseRepositoryCntxt<D> {
  datasource: D;

  protected constructor(datasource: D) {
    this.datasource = datasource;
  }
}

export default BaseRepository;
