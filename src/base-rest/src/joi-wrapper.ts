import DefaultJoi, { ObjectSchema, Root, SchemaMap } from 'joi';

const _Joi = DefaultJoi.defaults((schema) =>
  schema.options({
    presence: 'required',
    convert: true,
  })
);

const Joi: Root = {
  ..._Joi,
  boolean: () => _Joi.boolean().strict(),
  number: () => _Joi.number().strict(),
  string: () => _Joi.string().strict(),
  object: (schema?: SchemaMap) => _Joi.object(schema).unknown(true),
};
export { ObjectSchema };
export default Joi;
