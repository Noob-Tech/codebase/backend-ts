import { existsSync, readFileSync } from 'fs';
import * as _ from 'lodash';
import { join } from 'path';
import { BaseObject, LoggerCntxt, ConfigCntxt, ValidatorFn } from './types';

const LOG_SCOPE = 'API.Configuration';

class Config implements ConfigCntxt {
  logger!: LoggerCntxt;
  protected config: BaseObject;

  constructor(logger: LoggerCntxt) {
    this.logger = logger;
    this.config = {};
  }

  load(validator?: ValidatorFn, location?: string): void {
    const pwd = process.env.PWD || process.cwd();

    const configPath = location || join(pwd, 'config.json');

    let config: BaseObject;
    if (existsSync(configPath)) {
      const content = readFileSync(configPath, 'utf-8');
      config = JSON.parse(content);
    } else {
      this.logger.warn(`config file not found. Path=${configPath}`, {
        scope: LOG_SCOPE,
      });
      config = {};
    }

    if (validator) {
      validator(config);
    }

    this.config = config;
  }

  get(key: string) {
    const found = _.get(this.config, key);
    if (!found) throw new Error(`${LOG_SCOPE}: config ${key} not found`);
    return found;
  }
}

export default Config;
