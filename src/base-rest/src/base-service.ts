import { BaseAPICntxt, BaseServiceCntxt } from './types';

export default class BaseService<ReqT extends BaseAPICntxt = BaseAPICntxt> implements BaseServiceCntxt<ReqT> {
  appContext!: ReqT;

  protected constructor(appContext: ReqT) {
    if (!appContext.services) {
      appContext.services = {};
    }

    const serviceName = new.target.name;
    appContext.services[serviceName.charAt(0).toLowerCase() + serviceName.slice(1)] = this;

    this.appContext = appContext;
  }

  init() {
    throw new Error('Init method not implemented');
  }
}
