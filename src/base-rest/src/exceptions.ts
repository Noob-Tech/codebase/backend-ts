import { APIErrorCntxt, BaseObject, ResponseJSONObject } from './types';
import { StdCodes } from './responses';

export class APIError extends Error implements APIErrorCntxt {
  code: string;
  data: BaseObject = {};
  constructor(code: string | ResponseJSONObject, options: { message?: string; data?: BaseObject } = {}) {
    super(`APIError`);
    this.name = `APIError`;

    const { message, data } = options;
    this.data = data || {};

    if (typeof code === 'object') {
      this.code = code.code;
      this.message = message || code.message;
    } else {
      this.message = message || '';
      this.code = code;
    }
  }

  compose(message?: string, data?: BaseObject): APIErrorCntxt {
    this.message = message || this.message;
    this.data = data || this.data;
    return this;
  }
}

export const StdErrors = {
  ERROR_BAD_REQUEST: new APIError(StdCodes.ERROR_BAD_REQUEST, { message: 'Bad Request' }),
  ERROR_UNAUTHORIZED: new APIError(StdCodes.ERROR_UNAUTHORIZED, { message: 'Unauthorized' }),
  ERROR_FORBIDDEN: new APIError(StdCodes.ERROR_FORBIDDEN, { message: 'Forbidden' }),
  ERROR_INTERNAL: new APIError(StdCodes.ERROR_INTERNAL, { message: 'Internal Error' }),
  ERROR_BAD_GATEWAY: new APIError(StdCodes.ERROR_BAD_GATEWAY, { message: 'Bad Gateway' }),
};
