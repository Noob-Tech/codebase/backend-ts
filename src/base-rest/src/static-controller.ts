import { BaseObject, NtNext, NtRequest, NtResponse, ControllerArgs, BaseAPICntxt } from './types';
import { APIError } from './exceptions';
import { StdCodes } from './responses';
import BaseController from './base-controller';

const LOG_SCOPE = 'API.StaticController';

type StaticControllerArgs<T> = ControllerArgs<T> & { appName: string; appVersion: string };

export default class StaticController extends BaseController<BaseAPICntxt> {
  startTime: [number, number] = process.hrtime();
  meta: { appName: string; appVersion: string };

  constructor(args: StaticControllerArgs<BaseAPICntxt>) {
    super(args);

    this.meta = {
      appName: args.appName,
      appVersion: args.appVersion,
    };

    this.handleAPIStatus = this.handleAPIStatus.bind(this);
    this.handleResourceNotFound = this.handleResourceNotFound.bind(this);
    this.handleError = this.handleError.bind(this);
  }

  handleAPIStatus(request: NtRequest, response: NtResponse) {
    // Put request spec for logging
    response = BaseController.putResponseSpec(response, request);

    const elapsed = process.hrtime(this.startTime);
    const secs = elapsed[0];
    const minutes = Math.floor(secs / 60);
    const hours = Math.floor(minutes / 60);

    let uptime = '';
    if (hours > 0) uptime += `${hours}h `;

    if (minutes > 0) uptime += `${minutes}m`;

    uptime += `${secs}s ${elapsed[1] / 1000000}ms`;
    const data: BaseObject = {
      appName: this.meta.appName,
      appVersion: this.meta.appVersion,
      uptime,
    };
    this.sendSuccess(response, { data });
  }

  handleResourceNotFound(request: NtRequest, response: NtResponse) {
    // Put request spec for logging
    response = BaseController.putResponseSpec(response, request);

    this.sendError(response, new APIError(StdCodes.ERROR_NOT_FOUND));
  }

  handleError(err: any, request: NtRequest, response: NtResponse, next: NtNext) {
    // Put request spec for logging
    response = BaseController.putResponseSpec(response, request);

    this.logger.error(err, { scope: LOG_SCOPE });
    this.sendError(response, err);
  }
}
