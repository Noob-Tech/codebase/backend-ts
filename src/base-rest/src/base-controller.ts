import {
  APIErrorCntxt,
  BaseControllerCntxt,
  BaseObject,
  NtNext,
  NtRequest,
  NtResponse,
  HandlerResponse,
  IAsyncFn,
  IAsyncRESTFn,
  IRESTFn,
  LoggerCntxt,
  ResponseCntxt,
  ResponseData,
  ResponseMapperCntxt,
  RequiredParam,
  HandlerOptions,
  ControllerArgs,
  BaseAPICntxt,
} from './types';
import { StdCodes } from './responses';
import { APIError, StdErrors } from './exceptions';
import * as validators from './validators';

const LOG_SCOPE = 'API.Controller';

export default class BaseController<T extends BaseAPICntxt = BaseAPICntxt> implements BaseControllerCntxt<T> {
  responsesMapper!: ResponseMapperCntxt;
  debug: boolean;
  logger!: LoggerCntxt;
  appContext!: T;

  constructor(args: ControllerArgs<T>) {
    const { appContext, debug } = args;
    this.debug = debug || false;
    this.responsesMapper = appContext.responseMapper;
    this.logger = appContext.logger;

    if (!appContext.controllers) {
      appContext.controllers = {};
    }

    const controllerName = new.target.name;
    appContext.controllers[controllerName.charAt(0).toLowerCase() + controllerName.slice(1)] = this;

    this.appContext = appContext;
  }

  init() {
    throw new Error('Init method not implemented');
  }

  getRouter() {
    throw new Error('Get router method not implemented');
  }

  sendError(response: NtResponse, error: APIErrorCntxt, options: { logged?: boolean } = {}) {
    let errorData: BaseObject = {};
    let responseInterface: ResponseCntxt;
    if (error.name !== 'APIError') {
      responseInterface = this.responsesMapper.getInternalError();
    } else {
      responseInterface = this.responsesMapper.get(error.code, {});

      // Spread error data only on debug mode
      if (this.debug) errorData = { ...error.data };

      if (error.code !== '500' && responseInterface.code === StdCodes.ERROR_INTERNAL)
        this.logger.warn(`Error code not mapped. Code = ${error.code}`, {
          scope: LOG_SCOPE,
        });
    }

    // Log error & request
    if (options.logged || !this.debug) {
      this.logger.error(error);

      response._status = responseInterface.status;
      this.requestLogging(response);
    }

    const body: ResponseData = responseInterface.compose(error.message, errorData);
    response.status(responseInterface.status).json(body);
  }

  sendSuccess(response: NtResponse, data: HandlerResponse, options: { logged?: boolean } = {}) {
    const { code } = data;
    const responseInterface = this.responsesMapper.get(code || '200', {
      success: true,
    });
    const body: ResponseData = responseInterface.compose(data.message, data.data);

    // Log request
    if (options.logged || this.debug) {
      response._status = responseInterface.status;
      this.requestLogging(response);
    }

    response.status(responseInterface.status).json(body);
  }

  handlerAsync(asyncFn: IAsyncFn, options: HandlerOptions = {}) {
    return async (request: NtRequest, response: NtResponse, next: NtNext) => {
      try {
        // Add spec to response
        response = BaseController.putResponseSpec(response, request, options.trustProxy);

        // Validate parameters
        if (options.required) {
          const notValid = this.validateRequest(request, { params: options.required });
          if (notValid.length > 0)
            throw StdErrors.ERROR_BAD_REQUEST.compose('Some parameters are not valid/found', { notValid });
        }

        // Execute function
        await asyncFn(request, response, next);
      } catch (error) {
        this.sendError(response, error, { logged: options.logged });
      }
    };
  }

  handlerREST(RESTFn: IRESTFn, options: HandlerOptions = {}) {
    return (request: NtRequest, response: NtResponse) => {
      try {
        // Add spec to response
        response = BaseController.putResponseSpec(response, request);

        // Validate parameters
        if (options.required) {
          const notValid = this.validateRequest(request, { params: options.required });
          if (notValid.length > 0)
            throw StdErrors.ERROR_BAD_REQUEST.compose('Some parameters are not valid/found', { notValid });
        }

        // Execute function
        const result = RESTFn(request);

        this.sendSuccess(response, { ...result }, { logged: options.logged });
      } catch (err) {
        this.sendError(response, err, { logged: options.logged });
      }
    };
  }

  handlerAsyncREST(asyncRESTFn: IAsyncRESTFn, options: HandlerOptions = {}) {
    return async (request: NtRequest, response: NtResponse) => {
      try {
        // Add spec to response
        response = BaseController.putResponseSpec(response, request);

        // Validate parameters
        if (options.required) {
          const notValid = this.validateRequest(request, { params: options.required });
          if (notValid.length > 0)
            throw StdErrors.ERROR_BAD_REQUEST.compose('Some parameters are not valid/found', { notValid });
        }

        // Execute function
        const result = await asyncRESTFn(request);

        this.sendSuccess(response, { ...result }, { logged: options.logged });
      } catch (error) {
        this.sendError(response, error, { logged: options.logged });
      }
    };
  }

  validateRequest(request: NtRequest, options: { params?: RequiredParam[] }): RequiredParam[] {
    const notValid: RequiredParam[] = [];

    function checkParam(location: any, param: RequiredParam): boolean {
      const paramValue = location[param.param];
      if (param.options.required) {
        if (paramValue === undefined) return false;
      } else if (paramValue === undefined) return true;

      switch (param.type) {
        case 'boolean':
          return validators.booleanValidator(paramValue);
        case 'number':
          return validators.numberValidator(paramValue, {});
        case 'string':
          return validators.stringValidator(paramValue);
        case 'array':
          return validators.arrayValidator(paramValue);
        case 'date':
          return validators.dateValidator(paramValue);
        case 'email':
          return validators.emailValidator(paramValue);
        default:
          return false;
      }
    }

    const params: RequiredParam[] = options.params || [];
    params.forEach((param) => {
      switch (param.location) {
        case 'body':
          if (!checkParam(request.body, param)) notValid.push(param);
          break;
        case 'headers':
          if (!checkParam(request.headers, param)) notValid.push(param);
          break;
        case 'params':
          if (!checkParam(request.params, param)) notValid.push(param);
          break;
        case 'query':
          if (!checkParam(request.params, param)) notValid.push(param);
          break;
        default:
          throw StdErrors.ERROR_INTERNAL.compose('This parameter location is wrong', param);
      }
    });
    return notValid;
  }

  private requestLogging(response: NtResponse) {
    // Get response time
    let responseTime = 0;
    const hrTime = process.hrtime(response.requestTime || process.hrtime());

    // Add hrTime
    responseTime += hrTime[0] * 1000;
    responseTime += hrTime[1] / 1000000;

    this.logger.debug(
      `${response.remoteAddress} - ${response.method} ${response.originalUrl} ${
        response._status
      } - ${responseTime.toFixed(3)} ms`,
      { scope: LOG_SCOPE }
    );
  }

  static putResponseSpec(res: NtResponse, req: NtRequest, trustProxy?: boolean): NtResponse {
    res.method = req.method;
    res.originalUrl = req.originalUrl;
    res.remoteAddress = BaseController.getClientIP(req, trustProxy);
    res.requestTime = process.hrtime();
    return res;
  }

  static getClientIP(req: NtRequest, trustProxy?: boolean): string | undefined {
    // Get client IP from X-Real-IP header
    const clientIP = req.headers['x-real-ip'];

    if (clientIP) {
      if (Array.isArray(clientIP) && clientIP.length > 0) {
        return clientIP[0];
      }

      if (typeof clientIP === 'string') {
        return clientIP;
      }
    }

    // If trust proxy, get from x-forwarded-for
    if (trustProxy) {
      if (req.ip) {
        return req.ip;
      }

      if (req.ips && req.ips.length > 0) {
        return req.ips[0];
      }
    }

    return req.connection.remoteAddress;
  }
}
