import { SchemaMap } from 'joi';
import { ValidatorFn } from './types';
import { StdErrors } from './exceptions';
import Joi, { ObjectSchema } from './joi-wrapper';

export const booleanValidator = (value: boolean | string | number): boolean => {
  const result = Joi.boolean().validate(value);
  return !result.error;
};

export const emailValidator = (value: string): boolean => {
  const result = Joi.string().email().validate(value);
  return !result.error;
};

export const dateValidator = (value: string): boolean => {
  const result = Joi.string().isoDate().validate(value);
  return !result.error;
};

export const numberValidator = (value: number | string, options: { min?: number; max?: number }) => {
  const validator = Joi.number();

  if (options.min) {
    validator.min(options.min);
  }

  if (options.max) {
    validator.max(options.max);
  }

  const result = validator.validate(value);
  return !result.error;
};

export const arrayValidator = (value: any) => {
  const result = Joi.array().validate(value);
  return !result.error;
};

export const stringValidator = (value: any) => {
  const result = Joi.string().validate(value);
  return !result.error;
};

const schemaIsObjectSchema = (o: any): o is ObjectSchema => 'obs' in o;

export const createSchemaValidator = (schema: SchemaMap): ObjectSchema => Joi.object(schema);

export const createValidator = (schema: ObjectSchema | SchemaMap): ValidatorFn => {
  let validator: ObjectSchema;

  validator = Joi.isSchema(schema) ? (schema as ObjectSchema) : createSchemaValidator(schema as SchemaMap);

  return (value: any) => {
    const valid = validator.validate(value);
    if (valid.error) {
      throw StdErrors.ERROR_BAD_REQUEST.compose('Invalid parameters', valid.error.details);
    }
    return true;
  };
};
