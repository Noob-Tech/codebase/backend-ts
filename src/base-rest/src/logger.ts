import { kebabCase } from 'lodash';
import { BaseObject, LoggerCntxt, LoggerOptions } from './types';
import winston from 'winston';
import { APIError } from './exceptions';

// tslint:disable-next-line:max-classes-per-file
class Logger implements LoggerCntxt {
  logger!: winston.Logger;

  debugFlag: boolean = process.env.DEBUG === 'true';

  constructor(_serviceName: string) {
    _serviceName = kebabCase(_serviceName);

    let logLevel = process.env.LOG_LEVEL;
    switch (logLevel) {
      case 'debug':
      case 'error':
      case 'info':
      case 'warn':
        logLevel = logLevel.toLowerCase();
        break;
      default:
        logLevel = 'info';
        break;
    }

    const format = process.env.LOG_FORMAT;
    let logFormat;
    if (format === 'console') {
      logFormat = winston.format.combine(
        winston.format.timestamp({}),
        winston.format.colorize({ all: true }),
        winston.format.printf(
          ({ timestamp, level, message, metadata = '', stack = '', scope, serviceName }): string => {
            if (typeof metadata === 'object') {
              metadata = `\n> [metadata] ${JSON.stringify(metadata, null, 4)}`;
            }

            if (stack) {
              stack = `\n> [stackTrace] ${stack}`;
            }

            if (scope === undefined) {
              scope = serviceName;
            }

            return `[${timestamp}] ${level} - ${scope} \t| ${message}${metadata}${stack}`;
          }
        )
      );
    } else {
      logFormat = winston.format.combine(winston.format.timestamp(), winston.format.json());
    }

    this.logger = winston.createLogger({
      level: logLevel,
      format: logFormat,
      defaultMeta: {
        serviceName: _serviceName,
      },
      transports: [new winston.transports.Console()],
    });
  }

  info(message: string, options: LoggerOptions = {}) {
    this.logger.info(message, options);
  }

  warn(message: string, options: LoggerOptions = {}) {
    this.logger.warn(message, options);
  }

  debug(message: string, options: LoggerOptions = {}) {
    this.logger.debug(message, options);
  }

  error(error: any, options: LoggerOptions = {}) {
    if (error instanceof Error) {
      options.error = error;
      this.logger.error('Error', this.traceError(options));
    } else {
      this.logger.error(error, this.traceError(options));
    }
  }

  log(message: string, options?: BaseObject | number) {
    this.logger.log(message, options);
  }

  traceError(options?: LoggerOptions): BaseObject | void {
    if (!this.debugFlag) {
      return;
    }

    if (!options) {
      options = {};
    }

    if (!options.metadata) {
      options.metadata = {};
    }

    if (!options.error) {
      options.error = new Error();
    } else {
      if (options.error instanceof APIError) {
        Object.assign(options.metadata, options.error.data);
      }
    }

    return {
      stack: options.error.stack,
      metadata: options.metadata,
    };
  }
}

export default Logger;
