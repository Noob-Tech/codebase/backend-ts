import { ResponseMapperCntxt, BaseAPICntxt, ValidatorFn } from './contract';

export interface BaseObject extends Object {
  [key: string]: any;
}

export type ResponsesJSON = {
  [key: string]: ResponseJSONObject;
};

export type ResponseJSONObject = {
  code: string;
  status: number | string;
  message: string;
};

export type ResponseData = {
  code: string;
  message?: string;
  data?: BaseObject;
};

export type HandlerResponse = {
  code?: string;
  message?: string;
  data?: BaseObject;
};

export type ApiArgs = {
  responseMapper: ResponseMapperCntxt;
  configOpt?: {
    location?: string;
    validator?: ValidatorFn;
  };
  meta: {
    appName: string;
    appVersion: string;
  };
};

export type ControllerArgs<T> = {
  appContext: BaseAPICntxt & T;
  debug?: boolean;
};

export type RequiredParam = {
  param: string;
  location: 'params' | 'body' | 'headers' | 'query';
  type: 'boolean' | 'string' | 'number' | 'email' | 'array' | 'date';
  options: {
    required?: boolean;
  };
};

export type HandlerOptions = {
  logged?: boolean;
  required?: RequiredParam[];
  trustProxy?: boolean;
};

export type LoggerOptions = {
  scope?: string;
  error?: Error;
  metadata?: BaseObject;
};
