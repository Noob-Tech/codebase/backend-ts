import cors from 'cors';
import { ValidationError } from 'joi';
import {
  BaseObject,
  HandlerResponse,
  RequiredParam,
  ResponseData,
  ResponsesJSON,
  HandlerOptions,
  LoggerOptions,
} from './data';

// Wrap express app, req, resp, & next
import { Response, Request, NextFunction, Express, Router, RequestHandler } from 'express';
export type NtExpress = Express;
export interface NtResponse extends Response {
  method?: string;
  originalUrl?: string;
  remoteAddress?: string;
  requestTime?: [number, number];
  _status?: number;
}
export interface NtRequest extends Request {
  [key: string]: any;
}

export type NtNext = NextFunction;
export type NtRequestHandler = RequestHandler;

export type NtRouter = Router;
export type CorsOptions = cors.CorsOptions;

export type BaseAPICntxt = {
  app: NtExpress;
  appName: string;
  appVersion: string;
  responseMapper: ResponseMapperCntxt;
  logger: LoggerCntxt;
  config: ConfigCntxt;
  controllers?: BaseObject;
  services?: BaseObject;
  setup(): void;
  createRouter(): NtRouter;
  initComponents(): void;
  initDatasource(): void;
  initRepositories(): void;
  initServices(): void;
  initRouter(): NtRouter | void;
};

export type BaseControllerCntxt<T> = {
  appContext: T;
  responsesMapper: ResponseMapperCntxt;
  debug: boolean;
  logger: LoggerCntxt;

  init(): void;
  getRouter(): NtRouter | void;

  sendSuccess(response: NtResponse, data: HandlerResponse, options: { logged?: boolean }): void;
  sendError(response: NtResponse, error: APIErrorCntxt, options: { logged?: boolean }): void;

  handlerAsync: THandlerAsync;
  handlerREST: THandlerREST;
  handlerAsyncREST: THandlerAsyncREST;

  validateRequest(request: NtRequest, options: { params?: RequiredParam[] }): RequiredParam[];
};

export type BaseRepositoryCntxt<D> = {
  datasource: D;
};

export type BaseServiceCntxt<T> = {
  appContext: T;

  init(): void;
};

export type ConfigCntxt = {
  logger: LoggerCntxt;
  load(validator?: ValidatorFn, location?: string): void;
  get(key: string): any;
};

export interface APIErrorCntxt extends Error {
  code: string;
  data: BaseObject;
  compose(message?: string, data?: BaseObject): APIErrorCntxt;
}

export type ResponseCntxt = {
  code: string;
  status: number;
  message: string;
  data: BaseObject;
  compose(message?: string, data?: object): ResponseData;
};

export type ResponseMapperCntxt = {
  logger: LoggerCntxt;
  load(responses: ResponsesJSON): void;
  dump(location?: string): void;
  get(code: string, options: { success?: boolean }): ResponseCntxt;
  getInternalError(): ResponseCntxt;
  getSuccess(): ResponseCntxt;
};

export type LoggerCntxt = {
  debugFlag: boolean;

  log(message: string, options?: BaseObject | number): void;
  warn(message: string, options?: LoggerOptions): void;
  error(error: any, options?: LoggerOptions): void;
  info(message: string, options?: LoggerOptions): void;
  debug(message: string, options?: LoggerOptions): void;
  traceError(options?: LoggerOptions): BaseObject | void;
};

export type IAsyncFn = (request: NtRequest, response: NtResponse, next: NtNext) => Promise<void>;

export type IRESTFn = (request: NtRequest) => HandlerResponse | void;

export type IAsyncRESTFn = (request: NtRequest) => Promise<HandlerResponse | void>;

export type THandlerAsync = (asyncFn: IAsyncFn, options: HandlerOptions) => void;

export type THandlerREST = (RESTFn: IRESTFn, options: HandlerOptions) => void;

export type THandlerAsyncREST = (asyncRESTFn: IAsyncRESTFn, options: HandlerOptions) => void;

export type ValidatorFn = (value: any) => ValidationError | boolean;
