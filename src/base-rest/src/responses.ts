import { writeFileSync } from 'fs';
import { join } from 'path';
import { BaseObject, ResponseCntxt, ResponseData, ResponseMapperCntxt, ResponsesJSON, LoggerCntxt } from './types';

const LOG_SCOPE = 'API.Response';

export enum StdCodes {
  SUCCESS = '200',
  ERROR_BAD_REQUEST = '400',
  ERROR_UNAUTHORIZED = '401',
  ERROR_FORBIDDEN = '403',
  ERROR_NOT_FOUND = '404',
  ERROR_INTERNAL = '500',
  ERROR_BAD_GATEWAY = '502',
}

class Response implements ResponseCntxt {
  code: string;
  status: number;
  message: string;
  data: BaseObject = {};

  constructor(code: string, status: number, message: string) {
    this.code = code;
    this.status = status;
    this.message = message;
  }

  compose(message?: string, data?: object): ResponseData {
    return {
      code: this.code,
      message: message || this.message,
      data: data || this.data,
    };
  }
}

function initStandardResponses(): { [k: string]: ResponseCntxt } {
  return {
    [StdCodes.SUCCESS]: new Response(StdCodes.SUCCESS, 200, 'Success'),
    [StdCodes.ERROR_BAD_REQUEST]: new Response(StdCodes.ERROR_BAD_REQUEST, 400, 'Bad Request'),
    [StdCodes.ERROR_UNAUTHORIZED]: new Response(StdCodes.ERROR_UNAUTHORIZED, 401, 'Unauthorized'),
    [StdCodes.ERROR_FORBIDDEN]: new Response(StdCodes.ERROR_FORBIDDEN, 403, 'Forbidden'),
    [StdCodes.ERROR_NOT_FOUND]: new Response(StdCodes.ERROR_NOT_FOUND, 404, 'Not Found'),
    [StdCodes.ERROR_INTERNAL]: new Response(StdCodes.ERROR_INTERNAL, 500, 'Internal Error'),
    [StdCodes.ERROR_BAD_GATEWAY]: new Response(StdCodes.ERROR_BAD_GATEWAY, 502, 'Bad Gateway'),
  };
}

// tslint:disable-next-line:max-classes-per-file
export class ResponseMapper implements ResponseMapperCntxt {
  responses: { [k: string]: ResponseCntxt };
  logger!: LoggerCntxt;

  constructor(logger: LoggerCntxt) {
    this.logger = logger;
    this.responses = initStandardResponses();
  }

  load(responses: ResponsesJSON) {
    // Load custom response
    Object.keys(responses).forEach((key) => {
      const { code, message, status } = responses[key];

      // Convert to number if string
      let _status = status;
      if (typeof status === 'string') {
        _status = parseInt(status, 10);
      }

      this.responses[code] = new Response(code, _status as number, message);
    });
  }

  dump(location?: string) {
    location = location || process.cwd();
    const dumpData: ResponsesJSON = {};
    Object.keys(this.responses).forEach((code) => {
      const response = this.responses[code];
      dumpData[code] = {
        code,
        status: response.status,
        message: response.message,
      };
    });

    if (!location.endsWith('.json')) location = join(location, 'responses.json');
    this.logger.info(`Dump custom response to json at: ${location}`, {
      scope: LOG_SCOPE,
    });
    writeFileSync(location, JSON.stringify(dumpData));
    this.logger.info(`Custom response dumped`, { scope: LOG_SCOPE });
  }

  get(code: string, options: { success?: boolean }): ResponseCntxt {
    // Check if response exist by code
    if (this.responses.hasOwnProperty(code)) return this.responses[code];
    else {
      // If no success flag return internal error
      if (options.success) return this.getSuccess();
      else return this.getInternalError();
    }
  }

  getInternalError(): ResponseCntxt {
    return this.responses[StdCodes.ERROR_INTERNAL];
  }

  getSuccess(): ResponseCntxt {
    return this.responses[StdCodes.SUCCESS];
  }
}
