import { BucketItemStat, ItemBucketMetadata } from 'minio';

export interface MinIOStorages {
  [key: string]: MinIOStorage;
}

export type TMinioConfig = {
  endPoint: string;
  port: number;
  accessKey: string;
  secretKey: string;
  bucketName: string;
};

export type MinIOStorage = {
  addObject(objectName: string, file: Buffer, metaData?: ItemBucketMetadata): Promise<string>;
  getObject(objectName: string): Promise<{ file: Buffer; metadata: ItemBucketMetadata }>;
  getMetadata(objectName: string): Promise<BucketItemStat>;
};
