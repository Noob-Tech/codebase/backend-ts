import { Client, ItemBucketMetadata } from 'minio';
import { TMinioConfig, MinIOStorage, MinIOStorages } from './src/types';
const LOG_SCOPE = `STORAGE.MinIO`;

export class MinIO extends Client {
  config: TMinioConfig;
  protected storages: MinIOStorages;
  constructor(config: TMinioConfig) {
    super({
      endPoint: config.endPoint,
      accessKey: config.accessKey,
      secretKey: config.secretKey,
      port: config.port,
      useSSL: false,
    });

    this.bucketExists(config.bucketName, (err: Error | null, exist: boolean) => {
      if (err) throw err;
      if (!exist)
        this.makeBucket(this.config.bucketName, 'asia-southeast-1', (errMake: Error | null) => {
          if (errMake) throw errMake;
        });
    });

    this.config = config;
    this.storages = {};
  }

  protected define(storageName: string): MinIOStorage {
    return {
      addObject: async (objectName: string, file: Buffer, metadata: ItemBucketMetadata) =>
        this.putObject(this.config.bucketName, storageName + '/' + objectName, file, metadata),
      getMetadata: async (objectName: string) =>
        this.statObject(this.config.bucketName, storageName + '/' + objectName),
      getObject: async (objectName: string) => {
        const getObjectPromise = (): Promise<any[]> => {
          return new Promise((resolve, reject) => {
            const _fileArrayBuffer: any[] = [];
            this.getObject(this.config.bucketName, storageName + '/' + objectName, (err, { on }) => {
              if (err) return reject(err);
              on(`data`, (chunk) => _fileArrayBuffer.push(chunk));
              on(`end`, () => resolve(_fileArrayBuffer));
              on(`error`, (streamErr: Error) => reject(streamErr));
            });
          });
        };

        const fileArrayBuffer = await getObjectPromise();
        const fileStat = await this.statObject(this.config.bucketName, storageName + '/' + objectName);
        return {
          file: Buffer.concat(fileArrayBuffer),
          metadata: fileStat,
        };
      },
    };
  }

  createModel(storageName: string) {
    this.storages[storageName] = this.define(storageName);
  }

  getStorage(storageName: string): MinIOStorage {
    const storage = this.storages[storageName];
    if (!storage) throw new Error(`${LOG_SCOPE}: Storage ${storageName} not found`);
    return storage;
  }
}
