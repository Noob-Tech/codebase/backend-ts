import { DataType, ModelAttributeColumnOptions, Model, Sequelize, Options, ModelCtor, Transaction } from 'sequelize';

export interface NTSequelizeCntxt {
  sequelizeConn: Sequelize;
  createModel<T, U>(name: string, attributes: SqModelAttributes<T>, relations: SqModelRelation[]): void;
  associateAll(): void;
  getModel<T, U>(modelName: string): SqModel<T, U>;
  authenticate(): Promise<void>;
  createTransaction(): Promise<Transaction>;
}

export type SqModelAttribute = DataType | ModelAttributeColumnOptions;

export type TSequelizeConfig = Options;

export type SqModelAttributes<T extends { [key: string]: any }> = {
  [P in keyof T]: SqModelAttribute;
};

type ModelAssociate = (models: SqModels) => void;

export type SqModel<T extends { [key: string]: any }, U> = ModelCtor<Model<T, U>>;

export interface ISqModel<T, U> {
  associate?: ModelAssociate;
  model: SqModel<T, U>;
}

export type SqModels = {
  [key: string]: ISqModel<any, any>;
};

export type SqModelRelation = {
  modelName: string;
  relation: 'BELONGSTO' | 'HASONE' | 'HASMANY';
  options: {
    foreignKey?: string;
    as?: string;
    targetKey?: string;
    sourceKey?: string;
  };
};
