// tslint:disable:max-classes-per-file

import { DataTypes, Model, Op, Sequelize, Transaction } from 'sequelize';
import {
  NTSequelizeCntxt,
  SqModel,
  SqModelAttributes,
  SqModelRelation,
  SqModels,
  TSequelizeConfig,
  ISqModel,
} from './src/types';

const LOG_SCOPE = 'DS.Sequelize';

const stringIsNumber = (value: any) => !isNaN(Number(value));

export { Sequelize, DataTypes, Op, Transaction };

export function extractEnumKeys(enumName: any) {
  return Object.keys(enumName)
    .filter(stringIsNumber)
    .map((key) => enumName[key]);
}

export function extractEnumValues(enumName: any) {
  return Object.keys(enumName)
    .filter((key) => !stringIsNumber(key))
    .map((key) => enumName[key]);
}

export class NtSequelize implements NTSequelizeCntxt {
  sequelizeConn: Sequelize;
  protected models: SqModels;
  constructor(config: TSequelizeConfig) {
    this.sequelizeConn = new Sequelize(config);
    this.models = {};
  }

  createModel<T, U>(name: string, attributes: SqModelAttributes<T>, relations: SqModelRelation[]) {
    const newModel: ISqModel<T, U> = {
      model: this.sequelizeConn.define<Model<T, U>>(name, attributes, {
        timestamps: false,
        freezeTableName: true,
      }),
    };

    newModel.associate = function (models: SqModels) {
      relations.forEach((relation) => {
        const modelName = relation.modelName;

        const modelObject = models[modelName];
        if (!modelObject) throw new Error(`${LOG_SCOPE}: Model ${modelName} not found`);

        const relationModel = modelObject.model;

        let { foreignKey, as, targetKey, sourceKey } = relation.options;
        foreignKey = foreignKey || undefined;
        as = as || modelName;
        targetKey = targetKey || undefined;
        sourceKey = sourceKey || undefined;
        const _relation = relation.relation.toUpperCase();

        switch (_relation) {
          case 'BELONGSTO':
            this.model.belongsTo(relationModel, { foreignKey, as, targetKey });
            break;
          case 'HASMANY':
            this.model.hasMany(relationModel, {
              foreignKey,
              as,
              onDelete: 'no action',
              onUpdate: 'cascade',
              sourceKey,
            });
            break;
          case 'HASONE':
            this.model.hasOne(relationModel, {
              foreignKey,
              as,
              onDelete: 'no action',
              onUpdate: 'cascade',
              sourceKey,
            });
            break;
          default:
            break;
        }
      });
    };

    this.models[name] = newModel;
  }

  associateAll() {
    Object.keys(this.models).forEach((modelName) => {
      const model = this.models[modelName];
      if (model.associate) model.associate(this.models);
    });
  }

  getModel<T, U>(modelName: string): SqModel<T, U> {
    const model = this.models[modelName];
    if (!model) throw new Error(`${LOG_SCOPE}: Model ${modelName} not found`);

    return model.model;
  }

  async authenticate() {
    await this.sequelizeConn.authenticate();
  }

  async createTransaction(): Promise<Transaction> {
    return await this.sequelizeConn.transaction();
  }
}
